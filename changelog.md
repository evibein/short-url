# Evibe.in Shorten URL's

This change log will contain all the code changes for bug fixes, new features etc. Each version will contain sections: `Added`, `Changed`, `Removed`, `Fixed` (whichever is applicable).

### v1.0.1 (*23 Apr 2019*) `@JR`
##### Added
- Adding support for custom URL's

### v1.0.0 (*22 Mar 2019*) `@JR`
##### Added
- Shortening URL with all the access for admin